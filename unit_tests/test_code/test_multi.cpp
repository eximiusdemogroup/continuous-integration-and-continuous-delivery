#include "CppUTest/TestHarness.h"
#include <string.h>
#include <stdio.h>
#include <stdbool.h>

extern "C"
{
#include "multiplication.h"

}


TEST_GROUP(TestModule1)
{
    void setup()
    {

    }

    void teardown()
    {

    }
};

TEST(TestModule1, FirstTest)
{ 
    int a=multi(5,4);

    CHECK_EQUAL(20,a);
}
TEST(TestModule1, secondTest)
{ 
    int a=multi(-5,0);

    CHECK_EQUAL(0,a);
}
TEST(TestModule1, thirdTest)
{ 
    int a=multi(-5,-4);

    CHECK_EQUAL(20,a);
}
